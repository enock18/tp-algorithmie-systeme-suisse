import random
import json





players=[]
# Reading json file
with open('players_list.json', 'r') as openfile:
    players = json.load(openfile)
    

# We start by defining the function that will rank the players according to their elo points
def sort_by_elo(player_list)->None:
  player_list.sort(key=lambda x: x["elo_points"], reverse=True)

# We define the function that will simulate a round of the tournament
def round_similating(players)->None:
  # We start by sorting the players according to their elo points
  
  sort_by_elo(players)
  n = len(players)
  
  # We divide the players into two groups
  sub_group_1 = players[:n//2]
  sub_group_2 = players[n//2:]
  
  # On simule les matchs entre les joueurs de chaque sous-groupe
  for i in range(n//2):
    player_1 = sub_group_1[i]
    player_2 = sub_group_2[i]
    
    # We simulate the match and update the elo points of the players according to their performance
    result = simulate_match(player_1, player_2)
    if result == player_1["id"]:
      player_1["elo_points"] += 25
      player_2["elo_points"] -= 25
    elif result == player_1["id"]:
      player_1["elo_points"] -= 25
      player_2["elo_points"] += 25

# We define the function that will simulate a complete competition
def simulate_competition(player, round_br):
  for i in range(round_br):
    round_similating(player)

# We define the function that will simulate a match between two players
def simulate_match(player_1, player_2)->str:
 # We simulate the game using a random number
  if random.random() < 0.5:
    return player_1["id"]
  else:
    return player_2["id"]


# We simulate a tournament with 4 rounds
simulate_competition(players, 4)

# On affiche le classement final
sort_by_elo(players)

# write a new json file
with open("fight_statistique.json", "w") as outfile:
    json.dump(players, outfile)

for player in players:
  print(f'le joueur {player["first_name"]} a maintenant {player["elo_points"]}')


#print(players,"players")

